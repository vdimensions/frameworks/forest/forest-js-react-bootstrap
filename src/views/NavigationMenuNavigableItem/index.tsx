import React from 'react';
import { Nav } from 'react-bootstrap';
import { ForestView, useCommand } from '@vdimensions/forest-js-react';
import { NavigationNode } from '@vdimensions/forest-js-ui/Navigation';
import { ensureStartingSlash } from '../../utils/url-utils';

export default ForestView("NavigationMenuNavigableItem", (navItem: NavigationNode) => {
    const navigateCommand = useCommand("Navigate");
    const href = (navigateCommand && navigateCommand.path) ? ensureStartingSlash(navigateCommand.path) : undefined;
    const content = navItem.title || (navigateCommand && navigateCommand.displayName);
    return (
        <Nav.Item as="li">
            <Nav.Link
                href={href}
                onClick={ (e : any) => { e.preventDefault(); navigateCommand && navigateCommand.invoke(null); return false; } }
                disabled={navItem.selected}
                >
                {content}
            </Nav.Link>
        </Nav.Item>
    );
});
