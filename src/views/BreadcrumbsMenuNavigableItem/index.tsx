import React from 'react';
import { Breadcrumb } from 'react-bootstrap';
import { ForestView, useCommand } from '@vdimensions/forest-js-react';
import { NavigationNode } from '@vdimensions/forest-js-ui/Navigation';
import { ensureStartingSlash } from '../../utils/url-utils';

export default ForestView("BreadcrumbsMenuNavigableItem", (navItem: NavigationNode) => {
    const navigateCommand = useCommand("Navigate");
    const href = (navigateCommand && navigateCommand.path) ? ensureStartingSlash(navigateCommand.path) : undefined;
    const content = navItem.title || (navigateCommand && navigateCommand.displayName);
    return (
        <Breadcrumb.Item
            href={href}
            onClick={ (e: any) => { e.preventDefault(); navigateCommand && navigateCommand.invoke(null); } }
            >
            {content}
        </Breadcrumb.Item>
    );
});
