import React from 'react';
import { Nav } from 'react-bootstrap';
import { ForestView } from '@vdimensions/forest-js-react';
import { NavigationNode } from '@vdimensions/forest-js-ui/Navigation';

export default ForestView("NavigationMenuItem", (navItem: NavigationNode) => {
    let text = navItem.title || navItem.path;
    return (
        <Nav.Item as="li">
            <Nav.Link disabled={navItem.selected}>
                {text}
            </Nav.Link>
        </Nav.Item>
    );
});
