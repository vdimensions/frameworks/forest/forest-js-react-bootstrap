import React, {Dispatch, useState} from 'react';
import {Button, FormControl, InputGroup} from 'react-bootstrap';
import {FaCalendar} from "react-icons/fa";
import './index.scss';
import { getMonthName } from '../../utils/date-utils';
import Calendar, { DaysOfWeek } from '../Calendar';

type DatePickerMandatoryProps = {
    id: string
    name: string
    size?: 'sm' | 'lg';
}
type DatePickerOptionalProps = {
    className: string
    value: Date
    onChange: Dispatch<any>;
    minDate: Date;
    maxDate: Date;    
}

export type DatePickerProps = DatePickerMandatoryProps & Partial<DatePickerOptionalProps>;

class DatePickerState {
    readonly selectedDate?: Date;
    readonly calendarOpen: boolean = false;
    readonly monthName: string = "";
    
    constructor(props: DatePickerProps, data: any = {
        selectedDate: undefined,
        calendarOpen: false,
    }) {
        this.selectedDate = data.selectedDate;
        this.calendarOpen = data.calendarOpen;
        const monthFormat = props.size && props.size === "sm" 
            ? 'short' 
            : 'long';
        this.monthName = getMonthName(this.selectedDate, monthFormat);
    }
}

const DatePicker = (props: DatePickerProps) => {
    const [state, setState] = useState<DatePickerState>(new DatePickerState(props, {selectedDate: props.value || new Date()}));

    function handleDateSelect (d?: Date) {
        setState(new DatePickerState(props, {selectedDate: d, calendarOpen: false}));
        props.onChange && props.onChange({
            target: {
                name: props.name,
                value: d
            }
        });
    }
    
    return (
        <InputGroup className="date-picker" size={props.size}>
            {state.calendarOpen
            ? <Calendar
                firstDayOfWeek={DaysOfWeek.Monday}
                value={state.selectedDate}
                onDateChanged={handleDateSelect}
                minDate={props.minDate}
                maxDate={props.maxDate}
                />
            : <>
                <FormControl
                    type="text"
                    readOnly={true}
                    value={state.selectedDate ? (state.selectedDate).toLocaleDateString(window.navigator.language) : ''}
                    />
                <Button
                    variant="primary"
                    size={props.size}
                    onClick={(_: any) => setState(new DatePickerState(props, {...state, calendarOpen: true}))}
                    >
                    <FaCalendar/>
                </Button>
              </>
            }
        </InputGroup>
    );
};

export default DatePicker;
