﻿export type DateConstructor = { (
    year: number, 
    month: number,
    day: number,
    hour?: number,
    minute?: number,
    second?: number,
    millisecond?: number) : Date }

export const createUtcDate : DateConstructor = (
    year: number, 
    month: number,
    day: number,
    hour?: number,
    minute?: number,
    second?: number,
    millisecond?: number) => {
    
    return new Date(Date.UTC(year, month, day, (hour || 0), (minute || 0), (second || 0), (millisecond || 0)));    
}

export const createDate : DateConstructor = (
    year: number,
    month: number,
    day: number,
    hour?: number,
    minute?: number,
    second?: number,
    millisecond?: number) => {

    return new Date(year, month, day, (hour || 0), (minute || 0), (second || 0), (millisecond || 0));
}

export const getMonthName = (date: Date|undefined, monthFormat?: "short" | "long" | "numeric" | "2-digit" | "narrow") => {
    return date ? date.toLocaleString(window.navigator.language, {month: monthFormat}) : "";
};